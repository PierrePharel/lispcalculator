#ifndef RB_CHECKER_H
#define RB_CHECKER_H

#define QEmptyExpr -3
#define QIncorrectExpr -2
#define QUnknownVar -1
#define QGoodState 0
#define EMPTY_MSG ("\n\n")

#include <string.h>
#include <assert.h>
#include <gtk/gtk.h>
#include <ruby.h> 

extern VALUE m_check(VALUE expr);
extern void print_err_msg(const int state, GtkTextView *entry);

#endif