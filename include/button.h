#ifndef BUTTON_H
#define BUTTON_H

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdarg.h> 
#include <assert.h>

#define UTF8(str) g_locale_to_utf8(str, -1, NULL, NULL, NULL)
typedef struct
{
	GtkWidget **b_array;
	guint16 b_number;
} BArray;

extern void b_array_new(BArray *array, guint16 number ,gchar *label_array);
extern void b_array_new_with_label(BArray *array, gchar *label_array);
extern void  button_rows_gluing(GtkWidget *grid, guint16 left, guint16 top, GtkWidget* button, ...);
extern guint8 b_array_checker(BArray *array);
extern void button_clicked(GtkWidget *button, GtkTextView* entry);
extern gboolean b_equal_clicked(GtkWidget *equal, GtkTextView *entry);
extern void b_backspace_clicked(GtkWidget *erase,  GtkTextView* entry);
extern void b_clear_clicked(GtkWidget *clear,  GtkTextView* entryr);
extern void b_pow_clicked(GtkWidget *pow_2,  GtkTextView* entry);
extern void digit_button_gluing(BArray array, GtkWidget *grid);

#endif