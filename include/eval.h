#ifndef RB_EVAL_H
#define RB_EVAL_H

#include <gtk/gtk.h>
#include <ruby.h>

extern VALUE m_eval(VALUE parsed);
extern gchar *eval(gchar *expr);

#endif