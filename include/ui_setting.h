#ifndef UI_SETTING_H
#define UI_SETTING_H

#define WINDOW_W 378 
#define WINDOW_H 262

#include <assert.h>
#include <gtk/gtk.h>
#include <connector.h>

extern void header_bar_setting(GtkWidget *window, GtkWidget *header_bar);
extern void ui_setting(GtkWidget *window, GtkWidget *header_bar);
extern guint64 event_frames_refresh(EventFrames *object, const gchar *str, GtkTextBuffer *buffer);

#endif