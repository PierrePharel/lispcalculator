#ifndef BASIC_H
#define BASIC_H

#define DIGIT_BUTTONS 10
#define PUNCT_BUTTONS 3
/* signs chars and digits */
#define DIGIT_CHARS ("0123456789")
#define PUNCT_CHARS (".()")
#define DIVISION ("\u00F7")
#define SQUARE_ROOT ("\u221A") 
#define POW_2 ("\u00B2")
#define _POW_2 ("x\u00B2")
#define MODULO ("\x0025")
#define CLEAR ("\uFF23")
#define MULTIPLICATION ("\u00D7")
#define SUBTRACTION ("\u2212")
#define ADDITION ("\x002B")
#define EQUAL ("\x003D")

#include <stdio.h>
#include <gtk/gtk.h>
#include <stdarg.h>
#include <button.h>
#include <stddef.h>
#include <connector.h>
#include <assert.h>

extern EventFrames Gbl_Expr_List;
extern guint8 basic_ui(GtkWindow *window);
extern void char_truncate(const gchar *strsrc, gchar *strdest, guint8 index);

#endif