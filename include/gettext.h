#ifndef GETTEXT_H
#define GETTEXT_H
#include <libintl.h>

// I use "_g" to avoid a re-declaration because ruby c lib already use "_" keyword 
#define _g(STRING) gettext(STRING)

#endif