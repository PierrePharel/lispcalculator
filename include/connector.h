#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <gtk/gtk.h>

typedef struct
{
	GtkWidget **event_boxes;
	GtkWidget **frames;
	GtkWidget **labels; 
	GtkWidget *parent; 
	GtkCssProvider *provider;
} EventFrames;

#include <stdarg.h> 
#include <string.h>
#include <basic.h>
#include <button.h>
#include <parser.h>
#include <eval.h> 

// signal function(s)
extern void close_win(GtkWindow *window, gpointer user_data);
extern void c_button_buffer(GtkTextView *entry, BArray digits, BArray puncts, guint8 nb, ...);
extern void up_error_msg(GtkTextBuffer *buffer, GtkLabel *error_msg);
// entry function(s)
extern void entry_text_set(GtkTextView *entry, gchar *buffer_text, const int state);
extern gboolean entry_buffer_fill(GtkTextView *entry, GdkEventKey *event, gpointer user_data);
extern gboolean switch_string(GtkWidget *widget, GdkEventButton *event, GtkTextBuffer *buffer);

#endif