#ifndef RB_LOCALE_H
#define RB_LOCALE_H

extern const char *Gbl_Cur_Locale;

#include <locale.h>
#include <parser.h>

extern VALUE m_getcurloc(void);
extern VALUE m_applyloc(VALUE eval_str, VALUE locale);
extern VALUE m_revloc(VALUE str_expr, VALUE locale);

#endif