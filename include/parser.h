#ifndef RB_PARSER_H
#define RB_PARSER_H

#define WORK_DIR ("../src")
#define SUB ('-')
#define MUL ('*')
#define DIV ('/')
#define POW ('^')
#define SQRT ('r')
#define BLANK 0x20

#include <stdlib.h>
#include <unistd.h>
#include <ruby.h>
#include <gtk/gtk.h>
#include <string.h> 
#include <ctype.h>
#include <rb_locale.h>

extern VALUE m_add_wspace(VALUE expr);
extern VALUE m_cut_expr(VALUE expr_wspace);
extern VALUE parser(const gchar *expr);
extern void init_parser(void); 
extern void parse_uni_operator(gchar *str);
extern void	rm_unisub_op(gchar *str);
extern void	rm_unimul_op(gchar *str);
extern void	rm_unidiv_op(gchar *str);
extern void parser_pow_uni(gchar *str);
extern void parser_sqrt_uni(gchar *str);
extern size_t get_size(gchar *str);

#endif