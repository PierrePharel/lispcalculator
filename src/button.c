#include <basic.h>
#include <button.h>
#include <checker.h>
#include <ui_setting.h>

void b_array_new(BArray *array, guint16 number ,gchar *label_array)
{
	array->b_number = number;
	array->b_array = malloc(number * sizeof(GtkWidget));
	assert(array->b_array);
	b_array_new_with_label(array, label_array);
}

void b_array_new_with_label(BArray *array, gchar *label_array)
{
	gchar b_buffer[2];

	for(guint16 i = 0; i < array->b_number; ++ i)
	{
		char_truncate(label_array, b_buffer, i);
		array->b_array[i] = gtk_button_new_with_label(b_buffer);
	}
}

void button_rows_gluing(GtkWidget *grid, guint16 left, guint16 top, GtkWidget* button, ...)
{
	guint16 i = top; 
	va_list b_array;

	va_start(b_array, button);
	do
	{	
		gtk_grid_attach(GTK_GRID(grid), button, left, i, 1, 1);
		button = va_arg(b_array, GtkWidget*);
		++ i;
	}while(button != NULL);

	va_end(b_array);
}

guint8 b_array_checker(BArray *array)
{
	for(guint16 i = 0; i < array->b_number; ++ i)
	{
		if (array->b_array[i] == NULL)
			return 1;
	}

	return 0;
}

void button_clicked(GtkWidget *button, GtkTextView* entry)
{
	const gchar *str = gtk_button_get_label(GTK_BUTTON(button)); 
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(entry);

	gtk_text_buffer_insert_at_cursor(buffer, str, -1);
	gtk_widget_grab_focus(GTK_WIDGET(entry));
}

gboolean b_equal_clicked(GtkWidget *equal, GtkTextView *entry)
{
	GtkTextIter start, end;
	GtkTextBuffer *buffer;
	gchar *buffer_text;
	gchar *visible_text;
	guint64 i;
	
	buffer = gtk_text_view_get_buffer(entry);
	gtk_text_buffer_get_bounds(buffer, &start, &end);
	// preparing buffer set
	buffer_text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
	visible_text = gtk_text_buffer_get_text(buffer , &start, &end, FALSE);
	parse_uni_operator(buffer_text); 
	// expression state
	const int state = NUM2INT(m_check(rb_str_new_cstr(buffer_text)));
	if(state == QGoodState)
	{
		i = event_frames_refresh(&Gbl_Expr_List, visible_text, buffer);
		gtk_box_pack_end(GTK_BOX(Gbl_Expr_List.parent), Gbl_Expr_List.event_boxes[i], FALSE, FALSE, 10);
		gtk_widget_show_all(Gbl_Expr_List.parent);
	}

	entry_text_set(entry, buffer_text, state);
	gtk_widget_grab_focus(GTK_WIDGET(entry));
	
	return FALSE;
}

void b_backspace_clicked(GtkWidget *backspace,  GtkTextView* entry)
{
	GtkTextIter end;
	GtkTextBuffer *buffer;

	buffer = gtk_text_view_get_buffer(entry);
	gtk_text_buffer_get_end_iter(buffer, &end);
	gtk_text_buffer_backspace(buffer, &end, TRUE, TRUE);
	gtk_widget_grab_focus(GTK_WIDGET(entry));
}

void b_clear_clicked(GtkWidget *clear,  GtkTextView* entry)
{
	GtkTextIter start, end;
	GtkTextBuffer *buffer;

	buffer = gtk_text_view_get_buffer(entry);
	gtk_text_buffer_get_start_iter(buffer, &start);
	gtk_text_buffer_get_end_iter(buffer, &end);
	gtk_text_buffer_delete(buffer, &start, &end);
	gtk_widget_grab_focus(GTK_WIDGET(entry));
}

void b_pow_clicked(GtkWidget *pow_2,  GtkTextView* entry)
{
	GtkTextBuffer *buffer;

	buffer = gtk_text_view_get_buffer(entry);
	gtk_text_buffer_insert_at_cursor(buffer, POW_2, -1);
	gtk_widget_grab_focus(GTK_WIDGET(entry));
}

void digit_button_gluing(BArray array, GtkWidget *grid)
{
	guint16 start_n = 7;

	for(guint16 i = 3; i < 6; ++ i)
	{
		for(guint16  j = 0; j < 3; ++ j)
			gtk_grid_attach(GTK_GRID(grid), array.b_array[j + start_n], j, i, 1, 1);
		start_n -= 3;
	}

	gtk_grid_attach(GTK_GRID(grid), array.b_array[0], 0, 6, 1, 1);
}