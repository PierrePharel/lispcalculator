#include <connector.h>
#include <ui_setting.h>
#include <checker.h>

void close_win(GtkWindow *window, gpointer user_data)
{
	gtk_main_quit();
}

void c_button_buffer(GtkTextView *entry, BArray digits, BArray puncts, guint8 nb, ...)
{
	va_list buttons;
	GtkWidget **ary_button = malloc(sizeof(GtkWidget*) * nb);
	guint8 max = nb;

	va_start(buttons, nb);
	while(nb > 0)
	{	
		GtkWidget *button = va_arg(buttons, GtkWidget*);
		ary_button[(nb - max) * -1] = button;
		-- nb;
	}
	
	va_end(buttons);
	// digits
	g_signal_connect(G_OBJECT(digits.b_array[0]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[1]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[2]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[3]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[4]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[5]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[6]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[7]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[8]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(digits.b_array[9]), "clicked", G_CALLBACK(button_clicked), entry);
	// operators
	g_signal_connect(G_OBJECT(ary_button[0]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(ary_button[1]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(ary_button[2]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(ary_button[3]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(ary_button[4]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(ary_button[5]), "clicked", G_CALLBACK(button_clicked), entry);
	// puncts
	g_signal_connect(G_OBJECT(puncts.b_array[0]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(puncts.b_array[1]), "clicked", G_CALLBACK(button_clicked), entry);
	g_signal_connect(G_OBJECT(puncts.b_array[2]), "clicked", G_CALLBACK(button_clicked), entry);
	/* Special operators */
	// equal
	g_signal_connect(G_OBJECT(ary_button[6]), "clicked", G_CALLBACK(b_equal_clicked), entry);
	// pow_2
	g_signal_connect(G_OBJECT(ary_button[7]), "clicked", G_CALLBACK(b_pow_clicked), entry);
	// erase
	g_signal_connect(G_OBJECT(ary_button[8]), "clicked", G_CALLBACK(b_backspace_clicked), entry);
	// clear
	g_signal_connect(G_OBJECT(ary_button[9]), "clicked", G_CALLBACK(b_clear_clicked), entry);
}

gboolean entry_buffer_fill(GtkTextView *entry, GdkEventKey *event, gpointer user_data)
{
	GtkTextIter start, end;
	GtkTextBuffer *buffer;
	gchar *buffer_text;
	gchar *visible_text;
	guint64 i;

	if(event->keyval == GDK_KEY_Return)
	{
		event->keyval = GDK_KEY_VoidSymbol;
		buffer = gtk_text_view_get_buffer(entry);
		gtk_text_buffer_get_bounds(buffer, &start, &end);
		// preparing buffer set
		buffer_text = gtk_text_buffer_get_text(buffer , &start, &end, FALSE);
		visible_text = gtk_text_buffer_get_text(buffer , &start, &end, FALSE);
		parse_uni_operator(buffer_text); 
		// expression state
		const int state = NUM2INT(m_check(rb_str_new_cstr(buffer_text)));
		// visible buffer fill
		if(state == QGoodState)
		{
			i = event_frames_refresh(&Gbl_Expr_List, visible_text, buffer);
			gtk_box_pack_end(GTK_BOX(Gbl_Expr_List.parent), Gbl_Expr_List.event_boxes[i], FALSE, FALSE, 10);
			gtk_widget_show_all(Gbl_Expr_List.parent);
		}

		// buffer set
		entry_text_set(entry, buffer_text, state);
	}

	return FALSE;
}

void entry_text_set(GtkTextView *entry, gchar *buffer_text, const int state)
{
	gchar *result;
	GtkTextIter start, end;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(entry);

	if(state == QGoodState)
	{
		buffer = gtk_text_view_get_buffer(entry);
		result = eval(buffer_text);
		gtk_text_buffer_set_text(buffer, result, -1);
		gtk_text_buffer_get_bounds(buffer, &start, &end);
		gtk_text_buffer_apply_tag_by_name(buffer, "bold", &start, &end);
	}
	else
	{
		print_err_msg(state, entry);
	}

}

gboolean switch_string(GtkWidget *widget, GdkEventButton *event, GtkTextBuffer *buffer)
{
	const GtkWidget *child_frame = gtk_bin_get_child(GTK_BIN(widget));
	const GtkWidget *child_label = gtk_bin_get_child(GTK_BIN(child_frame));
	gchar *text_label = (gchar *)gtk_label_get_text(GTK_LABEL(child_label));
	gchar *p_str;

	// I will probably remake this part and include gstr array in expr_list struct
	// for minimal use of processes
	if(p_str = strchr(text_label, '\n'))
		memset(p_str, 0x00, 1);
	gtk_text_buffer_set_text(buffer, text_label, -1);

	return FALSE;
}

void up_error_msg(GtkTextBuffer *buffer, GtkLabel *error_msg)
{
	gtk_label_set_text(error_msg, EMPTY_MSG);
}