#!/usr/bin/env ruby -wKU
# encoding: UTF-8

LocaleFra = "fr"
LocaleEng = "en"
FraComma = ','
EngDot = '.'

def get_cur_loc(cur_loc_str)
	if cur_loc_str.include?(LocaleFra)
		return 2
	elsif cur_loc_str.include?(LocaleEng)
		return 1
	end

	return 0	
end

def rev_loc(str_expr, locale)
	decimal_point = EngDot
	result_revloc = ""

	if locale == 2
		str_expr.each_char { |c|
			if c == FraComma
				result_revloc += decimal_point
				next
			end

			result_revloc += c 
		}
	elsif locale == 1
		result_revloc = str_expr
	end

	return result_revloc
end

def apply_loc(eval_str, locale)
	decimal_point = EngDot
	result_locale = ""

	decimal_point = FraComma if locale == 2
	if locale != 0 && locale != -1
		eval_str.each_char { |c|
			if c == EngDot
				result_locale += decimal_point
				next
			end
			
			result_locale += c 
		}
	else
		result_locale = eval_str
	end

	return result_locale 
end