#!/usr/bin/env ruby -wKU
# encoding: UTF-8

class Integer
	def end?(ary)
		if self == ary.size  
			return true
		end

		return false
	end
end

def shrink_to_nine(str)
	shrinked_str = ""
	i = 0
	j = 0
	
	while i < str.size
		if i > str.index('.')
			j += 1
		end

		shrinked_str += str[i]
		break if j >= 9
		i += 1		
	end
	
	return shrinked_str
end