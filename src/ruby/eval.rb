#!/usr/bin/env ruby -wKU
# encoding: UTF-8

# Operators functions equivalent 
def add(foperand , soperand)
	return foperand.to_f + soperand.to_f
end

def sub(foperand , soperand)
	return foperand.to_f - soperand.to_f
end

def mul(foperand , soperand)
	return foperand.to_f * soperand.to_f
end

def div(foperand , soperand)
	return foperand.to_f / soperand.to_f
end

def mod(foperand , soperand)
	return foperand.to_f % soperand.to_f
end

def m_pow(foperand , soperand)
	return foperand.to_f ** soperand.to_f
end

def m_sqrt(operand)
	return Math.sqrt(operand.to_f)
end

# Macth str operators to good func equivalent 
def strto_op(foperand, operator, soperand = 0)
	result = 0

	case operator
	when AddChar
		result = add(foperand, soperand)
	when SubChar
		result = sub(foperand, soperand)
	when MulChar
		result = mul(foperand, soperand)
	when DivChar
		result = div(foperand, soperand)
	when ModChar
		result = mod(foperand, soperand) 
	when PowChar
		result = m_pow(foperand, soperand)
	when SqrtChar
		result = m_sqrt(foperand)
	end

	return result
end

# Recurse eval for sub special expression 
def recurse_eval(ary_expr, b = 0, e = 0)
	expr = ary_expr
	expr = ary_expr[b..e] if b != 0 && e != 0
	result = expr[0].to_f
	i = 1
	
	while i < expr.size
		if i % 2 == 0
			result = strto_op(result, expr[i - 1], expr[i])
		end

		i += 1
	end

	return result.to_s

end

# pow operator eval
def pow_m_eval(ary_expr)
	buffer = []

	ary_expr.each_index { |i|
		if ary_expr[i] != PowChar && ary_expr[i + 1] == PowChar
			buffer.push(strto_op(ary_expr[i], ary_expr[i + 1], 2).to_s)
		end
		next if ary_expr[i] != PowChar && ary_expr[i + 1] == PowChar
		buffer.push(ary_expr[i])
	}

	buffer.delete_if{ |str| str == PowChar }
	#puts "pow buffer : #{buffer}"

	return buffer
end
# square root eval 
def sqrt_m_eval(ary_expr)
	buffer = []

	ary_expr.each_index { |i|
		if ary_expr[i] == SqrtChar && ary_expr[i + 1] != SqrtChar
			buffer.push(strto_op(ary_expr[i + 1], ary_expr[i]).to_s)
		end
		next if ary_expr[i - 1] == SqrtChar && ary_expr[i] != SqrtChar
		buffer.push(ary_expr[i])
	}

	buffer.delete_if{ |str| str == SqrtChar }

	#puts "sqrt buffer : #{buffer}"

	return buffer
end
# Pow, Sqrt, Mul and Div priority eval
def priority_m_eval(ary_expr)
	first_buffer = []
	second_buffer = []
	j = 1
	endi = 0
	begini = 0
	
	# pow eval 
	ary_expr = pow_m_eval(ary_expr)
	# square root eval 
	ary_expr = sqrt_m_eval(ary_expr)
	if ary_expr.size >= 3
		# kind priority expr : 11*57+456/576 
		ary_expr.each_index { |i|
			if prio_operator?(ary_expr[i]) && !prio_operator?(ary_expr[i - 2]) && !prio_operator?(ary_expr[i + 2])
				endi = i + 2
				first_buffer.pop
				first_buffer.pop
				result_s = strto_op(ary_expr[i - 1], ary_expr[i], ary_expr[i + 1]).to_s 
				first_buffer.push(ary_expr[i - 2]) if i - 2 > 0
				first_buffer.push(result_s)
			end

			next if i < endi
			first_buffer.push(ary_expr[i])
		}

		endi = 0
		# kind priority expr : 11*57+456/576*13/46*5656 
		odd_i = odd_indexes(first_buffer)
		first_buffer.each_index { |i|
			if odd_i.size >= 2 && i % 2 == 0 && i == odd_i[j - 1]
				begini = odd_i[j - 1]
				endi = odd_i[j]
				result_s = recurse_eval(first_buffer, begini, endi)
				second_buffer.push(result_s) 
				j += 2 if j + 2 < odd_i.size 
			end

			next if odd_i.size >= 2 && i <= endi && begini > 0 && endi > 0
			second_buffer.push(first_buffer[i])
		}
	else
		second_buffer.push(ary_expr[0])
	end

	#puts "second_buffer : #{second_buffer}"

	return second_buffer
end

# Parenthesis sub expressions eval
def parenthesis_m_eval(ary_expr)
	buffer = []
	result_ary = []
	range_indexes = par_get_indexes(ary_expr)
	range_expr = []
	begini = 0
	endi = 0
	j = 1
	k = 0

	if range_indexes.size >= 2
			glob_par_indexes = indexfor_glob_par(ary_expr)
			ary_expr.each_index { |i|
			if i == range_indexes[j - 1]
				begini = range_indexes[j - 1]
				endi = range_indexes[j]
				# replace opened parenthesis by multiplication op for priority 
				range_expr = replace_parby_mul(begini, endi, ary_expr)
				# global parenthesis insert 
				if glob_par_indexes[k] == begini
					if range_expr.first == MulChar
						range_expr.insert(1, OpenParChar)
					else
						range_expr.insert(0, OpenParChar)
					end	
					k += 1
				end

				if (!operator?(range_expr.first) || range_expr.first == MulChar) &&\
				   ary_expr[begini] != CloseParChar && endi - begini >= 3
					b = 0
					b += 1 while range_expr[b] == MulChar || range_expr[b] == OpenParChar
					# eval sub expressions
					result = recurse_eval(priority_m_eval(range_expr[b..(endi - 1)]))
					range_expr = range_expr[0..(b - 1)]
					range_expr.push(result)
				end

				if glob_par_indexes[k] == endi
					range_expr.push(CloseParChar)
					k += 1
				end

				#puts "range_expr : #{range_expr}"
				buffer += range_expr
				j += 2 if j + 2 < range_indexes.size
			end 

			next if ((range_indexes[j - 3]..range_indexes[j - 2]).to_a.include?(i) ||\
					(range_indexes[j - 1]..range_indexes[j]).to_a.include?(i)) ||\
					(ary_expr[i] == OpenParChar || ary_expr[i] == CloseParChar)
			buffer.push(ary_expr[i]) 
		}

		# final eval of ary_expr
		range_indexes = par_get_indexes(buffer)
		j = 1
		buffer.each_index { |i|
			if i == range_indexes[j - 1]
				begini = range_indexes[j - 1]
				endi = range_indexes[j]
				range_expr = buffer[(begini + 1)..(endi - 1)]
				result = recurse_eval(priority_m_eval(range_expr))
				result_ary.push(result)
				j += 2 if j + 2 < range_indexes.size
			end

			next if ((range_indexes[j - 3]..range_indexes[j - 2]).to_a.include?(i) ||\
					(range_indexes[j - 1]..range_indexes[j]).to_a.include?(i)) ||\
					(buffer[i] == OpenParChar || buffer[i] == CloseParChar)
			result_ary.push(buffer[i]) 
		} 
	else
		result_ary = ary_expr
	end

	#buffer.delete_if { |str| str == OpenParChar || str == CloseParChar }

	return result_ary
end

# Main eval function after parse and eval of special operators like:
# - Pow, Sqrt
# - Priority operators(multiplication and division)
# kind expr for method : 1O+34-45+ ... (only eval of addition and subtraction) 
def m_eval(ary_expr) 
	ary_expr = priority_m_eval(parenthesis_m_eval(ary_expr))
	result = ary_expr[0].to_f

	# puts "#{ary_expr}"
	if ary_expr.size >= 3
		ary_expr.each_index { |i|
			if i >= 1 && i % 2 == 0
				result = strto_op(result, ary_expr[i - 1], ary_expr[i])
			end
		}
	else
		result = ary_expr[0].to_f
	end

	result = result.to_i if result == result.to_i
	result = result.to_s
	result = shrink_to_nine(result) if result.include?('.')
	puts "result : #{result}"
	
	return result
end