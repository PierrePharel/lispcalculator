#!/usr/bin/env ruby -wKU
# encoding: UTF-8

# Constants for signs and operators char 
AddChar = '+'
SubChar = '-'
MulChar = '*'
DivChar = '/'
ModChar = '%'
SpaceChar = ' '
OpenParChar = '('
CloseParChar = ')'
PowChar = '^'
SqrtChar = 'r'

def operator?(c)
	case c
	when AddChar, SubChar, MulChar, DivChar, ModChar, PowChar, SqrtChar
		return true
	end
	
	return false
end

def parenthesis?(c)
	case c
	when OpenParChar, CloseParChar
		return true
	end

	return false
end

def add_wspace(str)
	buffer = ""

	str.each_char { |c|
		if operator?(c) || parenthesis?(c)
			buffer += SpaceChar
			buffer += c
			buffer += SpaceChar
			next
		end

		buffer += c
	}

	return buffer
end

def cut_expr(str)
	ary_str = str.split(SpaceChar)
	#puts "ary_str : #{ary_str}"

	return ary_str
end

# Return true if operator is priority operator
# and false else
def prio_operator?(operator)
	case operator
	when MulChar, DivChar
		return true
	end

	return false
end

# Get indexes for continue priority operator like division or multiplication 
def odd_indexes(ary_expr)
	odd_i = []

	ary_expr.each_index { |i|
		if i >= 1 && i % 2 == 0
			puts ary_expr[i - 1] if prio_operator?(ary_expr[i - 1])
			odd_i.push(i) if !prio_operator?(ary_expr[i - 1]) && prio_operator?(ary_expr[i + 1])
			odd_i.push(i) if !prio_operator?(ary_expr[i + 1]) && prio_operator?(ary_expr[i - 1])
		end
	}

	return odd_i
end

def sub_parc?(begini, ary_expr)
	i = 0
	parenthesis_n = 0

	# Counter for parenthesis in range expression
	while i < begini
		parenthesis_n += 1 if parenthesis?(ary_expr[i])
		i += 1
	end

	i = begini
	while i < ary_expr.size
		if ary_expr[i] == CloseParChar || ary_expr[i] == OpenParChar &&\
		   (!parenthesis_n.even? && parenthesis_n > 0)
			return true	
		elsif ary_expr[i] == OpenParChar
			break
		end

		i += 1
	end

	return false
end

def sub_paro?(endi, ary_expr)
	i = 0
	parenthesis_n = 0

	# Counter for parenthesis in range expression
	while i < endi
		parenthesis_n += 1 if parenthesis?(ary_expr[i])
		i += 1
	end

	return true if parenthesis_n > 0
	return false
end

# Get parenthesis indexes 
def par_get_indexes(ary_expr)
	indexes = []

	ary_expr.each_index { |i|
		if ary_expr[i] == OpenParChar && ary_expr[i + 1] != OpenParChar
			indexes.push(i)
			indexes.push(i) if indexes.last == i && (!parenthesis?(ary_expr[i - 1]) &&\
			                   !parenthesis?(ary_expr[i + 1]) && i - 1 >= 0 &&\
			                   (!operator?(ary_expr[i - 1]) || ary_expr[i - 2] != CloseParChar)) &&\
			                   sub_paro?(i, ary_expr)
		end
	}

	ary_expr.each_index { |i|
		if ary_expr[i] == CloseParChar && (ary_expr[i - 1] != CloseParChar &&\
		   (operator?(ary_expr[i + 1]) || ary_expr[i + 1] == CloseParChar ||\
		   ary_expr[i + 1] != CloseParChar || (i + 1).end?(ary_expr)))
			indexes.push(i) 
			indexes.push(i) if indexes.last == i && operator?(ary_expr[i + 1]) &&\
				               sub_parc?(i + 1, ary_expr)
		end
	}

	indexes = indexes.sort

	return indexes
end

# Replace opened parenthesis by mutiplication operator
def replace_parby_mul(begini, endi, ary_expr)
	buffer = []
	ary_e = ary_expr[begini..endi]

	ary_e.each_index { |i|
		if ary_e[i] == OpenParChar && !operator?(ary_expr[begini - 1]) && begini > 0
			buffer.push(MulChar)
		end
		next if ary_e[i] == OpenParChar || ary_e[i] == CloseParChar
		buffer.push(ary_e[i])
	}

	buffer.pop if buffer.last == MulChar
	#puts "buffer : #{buffer}"
	return buffer
end

# Get indexes for final parenthesis for priority eval of sub parenthesis expressions 
def range_endtobegin(endi, ary_expr)
	parenthesis_n = {:opened => 0, :closed => 0}
	i = endi

	while i >= 0
		parenthesis_n[:opened] += 1 if ary_expr[i] == OpenParChar
		parenthesis_n[:closed] += 1 if ary_expr[i] == CloseParChar
		if (parenthesis_n[:opened] == parenthesis_n[:closed]) && (parenthesis_n[:opened] > 0 &&\
			parenthesis_n[:closed] > 0)
			return i
		end

		i -= 1
	end

	return 0
end

def indexfor_glob_par(ary_expr)
	indexes = []
	parenthesis_n = {:opened => 0, :closed => 0}

	ary_expr.each_index { |i|
		# ====> Begin to end
		# [0, 1, 2, 3, 4, 5, 6, 7, 8]
		parenthesis_n[:opened] += 1 if ary_expr[i] == OpenParChar
		parenthesis_n[:closed] += 1 if ary_expr[i] == CloseParChar
		if (parenthesis_n[:opened] == parenthesis_n[:closed]) && (parenthesis_n[:opened] > 0 &&\
			parenthesis_n[:closed] > 0)
			j = i
			j -= 1 while parenthesis?(ary_expr[j - 1])
			indexes.push(j)
			# End to begin          <====
			# [0, 1, 2, 3, 4, 5, 6, 7, 8]
			j = range_endtobegin(i, ary_expr)
			j += 1 while parenthesis?(ary_expr[j + 1])
			indexes.push(j)
			parenthesis_n[:closed] = 0
			parenthesis_n[:opened] = 0
		end

		#puts "#{parenthesis_n}"
	}

	indexes = indexes.sort

	return indexes
end