#!/usr/bin/env ruby -wKU
# encoding: UTF-8

# string bad alpha pattern
BadPatternAlpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
# string bad punct pattern 
BadPatternPunct = "!\"#$&':;<=>?@[\\]_`{|}~"
# arrar good digit pattern
GoodEachPattern = "1234567890"
EmptyExpr = -3
IncorrectExpr = -2
UnknownVar = -1
GoodState = 0

# check if parenthesis number are equal
def par_eq?(ary_expr)
	parenthesis_n = 0

	ary_expr.each { |e|
		parenthesis_n += 1 if parenthesis?(e)
	}

	return true if !parenthesis_n.odd? || parenthesis_n == 0
	return false
end

# count number of decimal dot in number
def dot(each_number)
	dot_n = 0

	each_number.each_char { |c|
		dot_n += 1 if c == FraComma || c == EngDot
	}

	return dot_n
end

# check dinvalid decimal
def invalid_decimal?(str_expr)
	ary_expr = cut_expr(add_wspace(str_expr))

	ary_expr.each { |e|
		if !operator?(e)
			return true if dot(e) > 1 
		end
	}

	return false 
end

# check for incomplet expression
def check_incomplet_expr?(str_expr)
	ary_expr = cut_expr(add_wspace(str_expr))
	size = ary_expr.size

	return true if (operator?(ary_expr.last) && ary_expr.last != PowChar) || !par_eq?(ary_expr) || ary_expr.first == PowChar
	ary_expr.each_index { |i|
		return true if (parenthesis?(ary_expr[i]) && parenthesis?(ary_expr[i + 1])) || (!GoodEachPattern.include?(ary_expr[i - 1]) && ary_expr[i] == PowChar)
	}

	return false
end

# ary for all punct in expression
def get_punct_ary(str_expr)
	punct_ary = []

	str_expr.each_char { |c|
		punct_ary.push(c) if BadPatternPunct.include?(c)
	}

	return punct_ary
end

# check if unknown operator is in expression 
def check_unknown_op?(str_expr)	
	return true if !get_punct_ary(str_expr).empty?
	return false
end

# check extended char is in expression
def check_ext_alpha?(str_expr)
	str_expr.each_char { |c|
		if !BadPatternAlpha.include?(c) && !GoodEachPattern.include?(c) && !BadPatternPunct.include?(c) && SpaceChar != c && !operator?(c) && !parenthesis?(c) && c != '.' && c != ','
			return true
		end
	}

	return false
end

def check_alpha?(str_expr)
	str_expr.each_char { |c|
		if BadPatternAlpha.include?(c) && c != SqrtChar
			return true
		end 
	}

	return false
end

# main check function
def check(str_expr)
	if check_alpha?(str_expr) || check_ext_alpha?(str_expr)
		return UnknownVar
	elsif check_unknown_op?(str_expr) || check_incomplet_expr?(str_expr) || invalid_decimal?(str_expr)
		return IncorrectExpr
	elsif str_expr.empty?
		return EmptyExpr	
	end

	return GoodState
end