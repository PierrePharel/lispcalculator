#include <eval.h>
#include <parser.h>
#include <rb_locale.h>
#include <checker.h>
const char *Gbl_Cur_Locale;

VALUE m_getcurloc(void)
{
	VALUE result;
	VALUE str_locale = rb_str_new_cstr(Gbl_Cur_Locale);
	
	result = rb_funcall(rb_mKernel, rb_intern("get_cur_loc"), 1, str_locale);

	return result;
}

VALUE m_revloc(VALUE str_expr, VALUE locale)
{
	VALUE result;

	result = rb_funcall(rb_mKernel, rb_intern("rev_loc"), 2, str_expr, locale);

	return result;
}

VALUE m_applyloc(VALUE eval_str, VALUE locale)
{
	VALUE result;

	result = rb_funcall(rb_mKernel, rb_intern("apply_loc"), 2, eval_str, locale);

	return result;
}

VALUE m_eval(VALUE parsed)
{
	VALUE result;

	result = rb_funcall(rb_mKernel, rb_intern("m_eval"), 1, parsed);

	return result;
}

gchar *eval(gchar *expr)
{
	VALUE parsed;
	VALUE result;

	parsed = parser(expr);
	result = m_eval(parsed);
	result = m_applyloc(result, m_getcurloc());
	
	return StringValueCStr(result);
}
