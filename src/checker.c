#include <checker.h>
#include <gettext.h>

VALUE m_check(VALUE expr)
{
	VALUE result;

	result = rb_funcall(rb_mKernel, rb_intern("check"), 1, expr);

	return result;
}

void print_err_msg(const int state, GtkTextView *entry)
{
	gchar *unknown_var = _g("\n\n    Unknown variable");
	gchar *incorrect_expr = _g("\n\n    Incorrect expression");
	GList *entry_child;
	GtkWidget *error_msg;
	GtkCssProvider *error_prov;

	entry_child = gtk_container_get_children(GTK_CONTAINER(entry));
	assert(entry_child);
	error_msg = (GtkWidget *)g_list_nth_data(entry_child, 0);
	gtk_widget_set_name(error_msg, "error-msg");
	error_prov = gtk_css_provider_new();
	gtk_css_provider_load_from_data(GTK_CSS_PROVIDER(error_prov), "label#error-msg { font-size : 14px; }", -1, NULL);
	gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(error_prov), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	if(state == QUnknownVar)
		gtk_label_set_text(GTK_LABEL(error_msg), unknown_var);
	else if(state == QIncorrectExpr)
		gtk_label_set_text(GTK_LABEL(error_msg), incorrect_expr);
}
