#include <stdio.h>
#include <gtk/gtk.h>
#include <basic.h>
#include <connector.h>
#include <parser.h>
#include <ui_setting.h>
#include <rb_locale.h>
#include <gettext.h>

int main(int argc, char **argv)
{
	GtkWidget *window;
	GdkPixbuf *icon;
	GtkWidget *header_bar;

	// locale setting
  	bindtextdomain ("lcalculator", getenv("PWD"));
  	textdomain ("lcalculator");
	Gbl_Cur_Locale = setlocale(LC_NUMERIC, "");
	/* GTK+ initialization */
	gtk_init(&argc, &argv);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	icon = gdk_pixbuf_new_from_file("../icons/lcalculator.png", NULL);
	gtk_window_set_icon(GTK_WINDOW(window), icon);
	if(!window)
	{
		fprintf(stderr, "Opening window failed.");
		exit(EXIT_FAILURE);
	}

	// ui setting
	ui_setting(window, header_bar);
	// Ruby VM init
	if(ruby_setup())
	{
		fprintf(stderr, "Failed to init Ruby VM.\n");
		exit(EXIT_FAILURE);
	}
	
	// Loading scripts for ruby expressions parser
	init_parser();
	// Launch Basic UI 
	if(basic_ui(GTK_WINDOW(window)))
	{
		fprintf(stderr, "Failed to start Basic UI.\n");
		exit(EXIT_FAILURE);
	}

	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(close_win), NULL);
	gtk_widget_show_all(window);
	gtk_main();

 	return ruby_cleanup(0);
}