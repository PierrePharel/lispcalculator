#include <parser.h>
#include <basic.h>

void init_parser(void)
{
	VALUE scripts[] = {rb_str_new_cstr("./ruby/parser.rb"), rb_str_new_cstr("./ruby/eval.rb"),\
						rb_str_new_cstr("./ruby/common.rb"), rb_str_new_cstr("./ruby/locale.rb"),\
						rb_str_new_cstr("./ruby/checker.rb")};
	int state = 0;
	chdir(WORK_DIR);
	
	for(int i = 0; i < 5; ++ i)
	{
		rb_load_protect(scripts[i], 0, &state);
		if(state)
		{
			fprintf(stderr, "Failed to init ruby VM and parser\n");
			exit(1);
		}
		
		state = 0;
	}
}

VALUE m_add_wspace(VALUE expr)
{
	VALUE result;

	result = rb_funcall(rb_mKernel, rb_intern("add_wspace"), 1, expr);

	return result;
}

VALUE m_cut_expr(VALUE expr_wspace)
{
	VALUE result;

	result = rb_funcall(rb_mKernel, rb_intern("cut_expr"), 1, expr_wspace);

	return result;
}

VALUE parser(const gchar *expr)
{
	VALUE rb_str_expr = m_revloc(rb_str_new_cstr(expr), m_getcurloc());
	VALUE expr_wspace;
	VALUE ary_expr;
	VALUE result;
	
	expr_wspace = m_add_wspace(rb_str_expr);
	ary_expr = m_cut_expr(expr_wspace);

	return ary_expr;
}

void rm_unisub_op(gchar *str)
{
	for(guint i = 0; i < strlen(str); ++ i)
		if(strstr(str + i, SUBTRACTION))
			memset(strstr(str + i, SUBTRACTION), SUB, 1);
}

void rm_unimul_op(gchar *str)
{
	for(guint i = 0; i < strlen(str); ++ i)
		if(strstr(str + i, MULTIPLICATION))
			memset(strstr(str + i, MULTIPLICATION), MUL, 1);
}

void rm_unidiv_op(gchar *str)
{
	for(guint i = 0; i < strlen(str); ++ i)
		if(strstr(str + i, DIVISION))
			memset(strstr(str + i, DIVISION), DIV, 1);
}

void parser_pow_uni(gchar *str)
{
	for(guint i = 0; i < strlen(str); ++ i)
	{
		char *ptr = NULL;

		if(ptr = strstr(str + i, POW_2))
		{
			memset(ptr, POW, 1);
			memset(ptr + 1, BLANK, 1);
		}
	}
}

void parser_sqrt_uni(gchar *str)
{
	for(guint i = 0; i < strlen(str); ++ i)
	{
		char *ptr = NULL;

		if(ptr = strstr(str + i, SQUARE_ROOT))
		{
			memset(ptr, SQRT, 1);
			memset(ptr + 1, BLANK, 2);
		}
	}
}

void rm_uni_left(gchar *str)
{
	for(guint i = 0; i < strlen(str); ++ i)
		if(!isdigit(str[i]) && !ispunct(str[i]) && str[i] != '^' && str[i] != 'r')
			str[i] = BLANK;
}

size_t get_size(gchar *str)
{
	size_t size = 0;

	for(guint i = 0; i < strlen(str); ++ i)
	{
		if(str[i] != BLANK)
			++ size;
	}

	return size;
}

void parse_uni_operator(gchar *str)
{
	size_t nw_size = get_size(str) + 1;
	gchar *nw_str = malloc(sizeof(gchar) * nw_size);
	guint j = 0;

	rm_unisub_op(str);
	rm_unimul_op(str);
	rm_unidiv_op(str);
	parser_pow_uni(str);
	parser_sqrt_uni(str);
	for(guint i = 0; i < strlen(str) + 1; ++ i)
	{
		if(str[i] != BLANK)
		{
			nw_str[j] = str[i];
			++ j;
		}
	}

	nw_str[nw_size - 1] = '\0';
	str = realloc(str, nw_size);
	for(guint i = 0; i < nw_size; ++ i) { str[i] = nw_str[i]; }
	str[nw_size - 1] = '\0';
	free(nw_str);
}