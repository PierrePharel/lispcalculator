#include <ui_setting.h>

void header_bar_setting(GtkWidget *window, GtkWidget *header_bar)
{
	header_bar = gtk_header_bar_new();
	assert(header_bar);
	// titlebar setting
	gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(header_bar), TRUE);
	gtk_header_bar_set_title(GTK_HEADER_BAR(header_bar), "LCalculator");
	gtk_window_set_titlebar(GTK_WINDOW(window), header_bar);
}

void ui_setting(GtkWidget *window, GtkWidget *header_bar)
{
	// setting default size for main window 
	gtk_window_set_default_size(GTK_WINDOW(window), WINDOW_W, WINDOW_H);
	// window title bar and header bar setting
	header_bar_setting(window, header_bar);
}
guint64 event_frames_refresh(EventFrames *object, const gchar *str, GtkTextBuffer *buffer)
{
	static guint64 i = 0;

	// memory re-allocation
	if(i <= 0)
	{
		// first init
		object->event_boxes = malloc(sizeof(GtkWidget *));
		object->frames = malloc(sizeof(GtkWidget *));
		object->labels = malloc(sizeof(GtkWidget *));
		assert(object->event_boxes);
		assert(object->frames);
		assert(object->labels);
		// resize font
		object->provider = gtk_css_provider_new();
		gtk_css_provider_load_from_path(object->provider, "../css-style/buffer_style.css", NULL);
		gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(object->provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	}
	else
	{
		// realloc or refresh size of sub-objects
		object->event_boxes = realloc(object->event_boxes, sizeof(GtkWidget *) * (i + 1));
		object->frames = realloc(object->frames, sizeof(GtkWidget *) * (i + 1));
		object->labels = realloc(object->labels, sizeof(GtkWidget *) * (i + 1));
	}
	// objects init
	object->event_boxes[i] = gtk_event_box_new();
	object->frames[i] = gtk_frame_new(NULL);
	object->labels[i] = gtk_label_new(str);
	// sub-objects settings
	gtk_frame_set_shadow_type(GTK_FRAME(object->frames[i]), GTK_SHADOW_NONE);
	gtk_widget_set_halign(object->frames[i], GTK_ALIGN_START);
	gtk_widget_set_hexpand_set(object->frames[i], TRUE);
	gtk_widget_set_hexpand(object->frames[i], FALSE);
	gtk_widget_add_events(object->event_boxes[i], GDK_BUTTON_PRESS_MASK);
	//gtk_widget_override_font(object->labels[i], object->pango_desc);
    gtk_label_set_ellipsize(GTK_LABEL(object->labels[i]), PANGO_ELLIPSIZE_END);
	// containers filling
	gtk_container_add(GTK_CONTAINER(object->frames[i]), object->labels[i]);
	gtk_container_add(GTK_CONTAINER(object->event_boxes[i]), object->frames[i]);
    g_signal_connect(G_OBJECT(object->event_boxes[i]), "button_press_event", G_CALLBACK(switch_string), buffer);
    ++ i;
    
    return i - 1;
}
