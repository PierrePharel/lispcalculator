#include <basic.h>
#include <checker.h>

EventFrames Gbl_Expr_List;

guint8 basic_ui(GtkWindow *window)
{
	GtkWidget *mother_grid;
	GtkWidget *button_grid;
	GtkWidget *entry_grid;
	GtkWidget *text_entry;
	GtkWidget *scrollable_entry;
	GtkWidget *scrollable_buffer;
	GtkWidget *frame_entry;
	GtkWidget *frame_buffer;
	GtkWidget *error_msg;
	GtkWidget *buffer_box;
	GtkWidget *modulo;
	GtkWidget *division;
	GtkWidget *subtraction;
	GtkWidget *multiplication;
	GtkWidget *add; 
	GtkWidget *backspace;
	GtkWidget *pow_2;
	GtkWidget *clear;
	GtkWidget *square_root;
	GtkWidget *equal;
	BArray digits;
	BArray puncts;
	GtkCssProvider *provider;
	GtkTextBuffer *buffer;

	// button struct init
	b_array_new(&digits, DIGIT_BUTTONS, DIGIT_CHARS);
	b_array_new(&puncts, PUNCT_BUTTONS, PUNCT_CHARS);
	if(b_array_checker(&digits) || b_array_checker(&puncts))
		exit(1);
	// each button init for special char and other math operator
	modulo = gtk_button_new_with_label(MODULO);
	division = gtk_button_new_with_label(DIVISION);
	multiplication = gtk_button_new_with_label(MULTIPLICATION);
	subtraction = gtk_button_new_with_label(SUBTRACTION);
	add = gtk_button_new_with_label(ADDITION);
	backspace = gtk_button_new();
	gtk_button_set_image(GTK_BUTTON(backspace), gtk_image_new_from_file("../icons/backspace.svg"));
	pow_2 = gtk_button_new_with_label(_POW_2);
	gtk_widget_set_name(pow_2, "pow-button");
	square_root = gtk_button_new_with_label(SQUARE_ROOT);
	equal = gtk_button_new_with_label(EQUAL);
	gtk_widget_set_name(equal, "equal-button");
	clear = gtk_button_new_with_label(CLEAR);
	// container (tables and window) init
	mother_grid = gtk_grid_new();
	entry_grid = gtk_grid_new();
	button_grid = gtk_grid_new();
	frame_entry = gtk_frame_new(NULL);
	frame_buffer = gtk_frame_new(NULL);
	// scrollable win for frames (buffer and entry)
	scrollable_entry = gtk_scrolled_window_new(NULL, NULL);
	scrollable_buffer = gtk_scrolled_window_new(NULL, NULL);
	// text view setting
	error_msg = gtk_label_new(EMPTY_MSG);
	text_entry = gtk_text_view_new();
	gtk_container_add(GTK_CONTAINER(text_entry), error_msg);
	gtk_text_view_set_left_margin(GTK_TEXT_VIEW(text_entry), 14);
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_entry));
	// buffer box setting
	buffer_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_set_name(buffer_box, "buffer-box");
	// visible buffer's container filling
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollable_buffer), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_widget_set_halign(buffer_box, GTK_ALIGN_START);
	gtk_widget_set_margin_start(buffer_box, 12);
	gtk_container_add(GTK_CONTAINER(scrollable_buffer), buffer_box);
	gtk_container_add(GTK_CONTAINER(frame_buffer), scrollable_buffer);
	// Special scrollable entry build
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollable_entry), GTK_POLICY_AUTOMATIC, GTK_POLICY_NEVER);
	gtk_container_add(GTK_CONTAINER(scrollable_entry), text_entry);
	gtk_container_add(GTK_CONTAINER(frame_entry), scrollable_entry);
	// Organization of frames in tables for main entry and buffer
	// button grid set up
	gtk_grid_set_column_spacing(GTK_GRID(button_grid), 4);
	gtk_grid_set_row_spacing(GTK_GRID(button_grid), 4);
	gtk_grid_set_column_homogeneous(GTK_GRID(button_grid), TRUE);
	gtk_grid_set_row_homogeneous(GTK_GRID(button_grid), TRUE);
	// buttons gluing or attach in tables
	digit_button_gluing(digits, button_grid);
	gtk_grid_attach(GTK_GRID(button_grid), puncts.b_array[0], 1, 6, 1, 1);
	gtk_grid_attach(GTK_GRID(button_grid), modulo, 2, 6, 1, 1);
	button_rows_gluing(button_grid, 3, 3, division, multiplication, subtraction, add, NULL);
	button_rows_gluing(button_grid, 4, 3, backspace, puncts.b_array[1], pow_2, NULL);
	button_rows_gluing(button_grid, 5, 3, clear, puncts.b_array[2], square_root, NULL);
	gtk_grid_attach(GTK_GRID(button_grid), equal, 4, 6, 2, 1);
	// mother grid setting
	gtk_grid_set_row_spacing(GTK_GRID(mother_grid), 4);
	gtk_grid_set_column_homogeneous(GTK_GRID(mother_grid), TRUE);
	gtk_grid_set_row_homogeneous(GTK_GRID(mother_grid), TRUE);
	gtk_container_set_border_width(GTK_CONTAINER(mother_grid), 15);
	// child tables attach on mother table
	gtk_grid_attach(GTK_GRID(mother_grid), frame_buffer, 0, 0, 6, 5);
	gtk_grid_attach(GTK_GRID(mother_grid), frame_entry, 0, 5, 6, 3);
	gtk_grid_attach(GTK_GRID(mother_grid), button_grid, 0, 8, 6, 6);
	// grad keyboard focus to text_entry
	if(gtk_widget_get_can_focus(text_entry));
		gtk_widget_grab_focus(text_entry);
	// css style applying
	provider = gtk_css_provider_new();
	gtk_css_provider_load_from_path(provider, "../css-style/main_style.css", NULL);
	gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	// tag creating
	gtk_text_buffer_create_tag(buffer, "bold", "weight", PANGO_WEIGHT_BOLD, NULL);
	// visible buffer list parent set
	Gbl_Expr_List.parent = buffer_box;
	// mother_table add on window 
	gtk_container_add(GTK_CONTAINER(window), mother_grid);
	// Signals
	c_button_buffer(GTK_TEXT_VIEW(text_entry), digits, puncts, 10, modulo, division, multiplication, subtraction, add, square_root, equal, pow_2, backspace, clear);
	g_signal_connect(G_OBJECT(text_entry), "key-press-event", G_CALLBACK(entry_buffer_fill), buffer_box);
	g_signal_connect(G_OBJECT(buffer), "changed", G_CALLBACK(up_error_msg), error_msg);
	// release of all allocated objects
	free(digits.b_array);
	free(puncts.b_array);
	free(Gbl_Expr_List.event_boxes);
	free(Gbl_Expr_List.frames);	
	free(Gbl_Expr_List.labels);	

    return EXIT_SUCCESS;
}

void char_truncate(const gchar *strsrc, gchar *strdest, guint8 index)
{
	strdest[0] = strsrc[index]; 
	strdest[1] = '\0';
}