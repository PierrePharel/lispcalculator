LCalculator
============
Lcalculator is for "LispCalculator". It's an fast and lightweight calculator write in C for the graphic interface with GTK+3 and Ruby for parsing and evaluating of expressions , whith an basic mode (inspired by basic mode of gnome calculator) and an lisp mode, in which you can evaluate naturaly lisp expression like : **(+ 1 2)**.

For lisp evaluating we will use [ArkScipt](https://github.com/ArkScript-lang/Ark) by [SuperFola](https://github.com/SuperFola) REPL.

Dependencies
------------
*  CMake >= 3.10
*  GCC 7
*  GTK+3
*  Ruby-dev library
*  Intl & GNU gettext (only if you would recompile translate files (.pot, .po and .mo))

Building
------------
```bash
# clone repo
~$ git clone https://gitlab.com/PierrePharel/lispcalculator.git
# building
~/lispcalculator$ mkdir bin
~/lispcalculator$ cmake -H. -Bbin
~/lispcalculator$ cmake --build bin
```
For translate files, in **bin** directory create this tree :	**_lang_/LC_MESSAGES**, and paste .mo file

Screen
------------
A screen of basic mode     

![basic mode](./screens/basic_mode.png) 

_Lisp mode should become soon_